//
//  AppDelegate.h
//  so
//
//  Created by Xu Jun on 12/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenGLView.h"

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    OpenGLView* m_glView;
}
@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@property (nonatomic, retain) IBOutlet OpenGLView *glView;
@end
